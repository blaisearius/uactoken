                                 UACToken

   PRE REQUIS, CONFIGURATION ET TEST

   1. Pré-requis.

      "Node JS" : une plateforme logicielle libre en JavaScript orientée vers 
      les applications réseau événementielles hautement concurrentes qui 
      doivent pouvoir monter en charge.

      "Truffle Framework" un environnement de développement et un cadre de test 
      et un pipeline d'actifs pour les chaînes de blocs utilisant la 
      machine virtuelle Ethereum (EVM).

      "Ganache" : utilitaire pour créer une blockchain victive en local.

   2. Une fois Ganache installé, créer une blockchain locale 
   (voir https://www.trufflesuite.com/ganache)

   3. Pour exécuter les tests unitaires la commande "truffle test" sera utilisée. 
   Se référer à la documentation officielle pour les commandes usuelles 
   (voir https://www.trufflesuite.com/docs/truffle/overview)

   4. Test du Smart contract via une plateforme en ligne

      (a) Se rendre sur la plateforme Remix - Ethereum IDE https://remix.ethereum.org/ 

      (b) Ajouter le Smart Contract UACToken (/contracts/UACToken.sol)

      (c) Compiler et déployer le smart contract

      (d) Tester les fonctions disponibles : name, symbol, balanceOf, transfer

   
   
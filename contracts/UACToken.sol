// SPDX-License-Identifier: MIT
pragma solidity >=0.4.21 <0.7.0;

/**
 * @title Implementation of the ERC223 standard token.
 */
contract UACToken{

    /**
     * @dev The total supply of the token.
     */
    uint256 public totalSupply;

    /**
     * @dev The name of the token.
     */
    string public name;

    /**
     * @dev The symbol of the token.
     */
    string public symbol;

    /**
     * @dev The Admin Address.
     */
    address public adminAddress;

    // mapping that set "balances" as list of user balances
    mapping(address => uint256) balances;

    modifier restrictedToAdmin() {
        if(msg.sender == adminAddress) _;
    }

    event Transfer(
        address indexed _from,
        address indexed _to,
        uint256 _value
    );

    constructor(uint256 _initialSupply) public {
        name = "UAC Token";
        symbol = "UACT";
        totalSupply = _initialSupply;
        adminAddress = msg.sender;
        balances[adminAddress] = totalSupply;
    }

    /**
     * @dev Transfer the specified amount of tokens to the specified address.
     *
     * @param _to    Receiver address.
     * @param _value Amount of tokens that will be transferred.
     */
    function transfer(address _to, uint256 _value) public returns (bool success){
        require(balances[msg.sender] >= _value, 'insufficient balance for transfer');
        balances[msg.sender] = balances[msg.sender] - (_value);
        balances[_to] = balances[_to] + (_value);

        // emit event
        emit Transfer(msg.sender, _to, _value);

        return true;
    }

    /**
     * @dev Returns balance of the `_owner`.
     *
     * @param _owner   The address whose balance will be returned.
     * @return balance Balance of the `_owner`.
     */
    function balanceOf(address _owner) public view returns (uint256 balance) {
        return balances[_owner];
    }

}